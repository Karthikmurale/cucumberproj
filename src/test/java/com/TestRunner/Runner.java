package com.TestRunner;

import cucumber.api.CucumberOptions;
import cucumber.api.Scenario;
import cucumber.api.testng.AbstractTestNGCucumberTests;



@CucumberOptions(features = "Features/nopScenarios.feature", glue = "com.StepDefinitions", dryRun = false, monochrome = true,tags= { "@table" }, plugin = {
		"pretty", "html:target/htmlreports","json:target/cucumber.json","html:target/site/cucumber-pretty" })

public class Runner extends AbstractTestNGCucumberTests {
	
	
	
} 
