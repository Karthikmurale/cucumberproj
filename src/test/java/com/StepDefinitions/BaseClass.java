package com.StepDefinitions;

import org.openqa.selenium.WebDriver;

import com.Utilities.UsefulMethods;
import com.aventstack.extentreports.ExtentReports;
import com.aventstack.extentreports.ExtentTest;
import com.aventstack.extentreports.reporter.ExtentHtmlReporter;

public class BaseClass {
	
	public static WebDriver driver;
	public static ExtentHtmlReporter reporter;
	public static ExtentReports report;
	public static ExtentTest test;
	
	
	public static void eReport(String Status,String Details)
	{
		try
		{
			if(Status.equalsIgnoreCase("PASS"))
			{
				test.log(com.aventstack.extentreports.Status.PASS, Details +test.addScreenCaptureFromPath(UsefulMethods.takeSnap(driver)));
			}
			else if(Status.equalsIgnoreCase("FAIL"))
			{
				test.log(com.aventstack.extentreports.Status.FAIL, Details +test.addScreenCaptureFromPath(UsefulMethods.takeSnap(driver)));
			}
			else if(Status.equalsIgnoreCase("INFO"))
			{
				test.log(com.aventstack.extentreports.Status.INFO, Details +test.addScreenCaptureFromPath(UsefulMethods.takeSnap(driver)));
			}
	}
		catch(Exception e)
		{
			System.out.println("Exception is "+e);
		}
	}

}
