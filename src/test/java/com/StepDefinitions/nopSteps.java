package com.StepDefinitions;
import java.io.File;
import org.openqa.selenium.By;
import org.openqa.selenium.interactions.Actions;
import com.PageObjects.NOPHomePage;
import com.PageObjects.NOPLoginPage;
import com.PageObjects.Register;
import com.TestRunner.Runner;
import com.Utilities.UsefulMethods;
import com.aventstack.extentreports.Status;

import cucumber.api.DataTable;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import gherkin.formatter.model.Scenario;

public class nopSteps extends BaseClass {

	@Given("^user login to the nopCommerce WebApplication$")
	public void user_login_to_the_nopCommerce_WebApplication() throws Throwable {
		
		test = report.createTest("Login Functionality");
		NOPLoginPage noplogin = new NOPLoginPage(driver);
		eReport("Pass", "Reached the Login Screen");
		noplogin.enterUsername(UsefulMethods.getConfigdata("username"));
		noplogin.enterPassword(UsefulMethods.getConfigdata("password"));
		noplogin.btnClick();
		eReport("Pass", "Login button clicked successfully");
		System.out.println("Login button clicked successfully");
	}

	@When("^verify user should reach the homepage$")
	public void verify_user_should_reach_the_homepage() throws Throwable {
		test = report.createTest("HomePage Verification");
		String eTitle = "nopCommerce - Free and open-source eCommerce platform. ASP.NET based shopping cart.";
		if (driver.getTitle().equalsIgnoreCase(eTitle)) {
			System.out.println("User Navigated to Home page successfully & Test case got Passed:-- " + eTitle);
		}
		eReport("Pass", "User Navigated to Home page successfully & Test case got Passed");
	}

	@When("^user should logout to the nopCommerce Application$")
	public void user_should_logout_to_the_nopCommerce_Application() throws Throwable {
		test = report.createTest("Logout Functionality");
		NOPHomePage nopHome = new NOPHomePage(driver);
		Actions action = new Actions(driver);
		driver.findElement(By.xpath("//span[@class='ico-caret sprite-image']")).click();
		action.moveToElement(driver.findElement(By.xpath("//span[@class='ico-caret sprite-image']"))).build().perform();;
		nopHome.logoutClick();
		Thread.sleep(3000);
		eReport("Pass", "Logout button Clicked Successfully");
		System.out.println("Logout button Clicked Successfully");
		report.flush();
	}
	
	
	@Given("^User navigates to Login Screen$")
	public void user_navigates_to_Login_Screen() throws Throwable {
		
		System.out.println("User navigates to Login screen");
		UsefulMethods.takeSnap(driver);
	}

	@When("^User clicks on the Register link$")
	public void user_clicks_on_the_Register_link() throws Throwable {
		
		Register reg = new Register(driver);
		reg.clickRegister();
		System.out.println("Register link has been clicked");
		UsefulMethods.takeSnap(driver);
	}

	@Then("^User should be navigated to Register Screen$")
	public void user_should_be_navigated_to_Register_Screen() throws Throwable {
		Register reg = new Register(driver);
		System.out.println("The page name displayed is "+reg.titleName());
		UsefulMethods.takeSnap(driver);
	}

	@Then("^User enters all the required datas$")
	public void user_enters_all_the_required_datas(DataTable table) throws Throwable {
	    // Write code here that turns the phrase above into concrete actions
	    // For automatic transformation, change DataTable to one of
	    // List<YourType>, List<List<E>>, List<Map<K,V>> or Map<K,V>.
	    // E,K,V must be a scalar (String, Integer, Date, enum etc)
		Register reg = new Register(driver);
		reg.enterFirstName(table.raw().get(1).get(0));
		reg.enterLastName(table.raw().get(1).get(1));
		reg.enterEmail(table.raw().get(1).get(2));
		reg.enterConfirmEmail(table.raw().get(1).get(3));
		reg.enterUsername(table.raw().get(1).get(4));
		UsefulMethods.takeSnap(driver);
	}

	@Then("^User should click on Check Availability$")
	public void user_should_click_on_Check_Availability() throws Throwable {
		Register reg = new Register(driver);
		reg.clickCheckAvailability();
		System.out.println("Check Availability button is clicked");
		UsefulMethods.takeSnap(driver);
	}

}
