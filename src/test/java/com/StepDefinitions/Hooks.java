package com.StepDefinitions;

import java.io.File;
import java.io.IOException;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.chrome.ChromeDriver;

import com.Utilities.UsefulMethods;
import com.aventstack.extentreports.reporter.ExtentHtmlReporter;

import cucumber.api.Scenario;
import cucumber.api.java.After;
import cucumber.api.java.Before;

public class Hooks extends BaseClass {

	@Before
	public void setup() throws IOException
	{
		System.setProperty("webdriver.chrome.driver",
				"E:\\Selenium Workspace\\Driver\\79\\chromedriver.exe");
		driver = new ChromeDriver();
		driver.get(UsefulMethods.getConfigdata("url"));
		driver.manage().window().maximize();
		driver.manage().timeouts().implicitlyWait(5000, TimeUnit.SECONDS);
		System.out.println("URL Launched Successfully");
		String path= System.getProperty("user.dir")+File.separator+"ExtentReport"+File.separator+"AutomationReport.html";
		reporter = new ExtentHtmlReporter(path);
		report = new com.aventstack.extentreports.ExtentReports();
		report.attachReporter(reporter);
		
	}
	
	@After
	public void teardown()
	{
		
		report.flush();
		driver.close();
		driver.quit();
	}
}
