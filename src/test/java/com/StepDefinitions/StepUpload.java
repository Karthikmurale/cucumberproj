package com.StepDefinitions;

import java.awt.Robot;
import java.awt.Toolkit;
import java.awt.datatransfer.StringSelection;
import java.awt.event.KeyEvent;
import com.PageObjects.UploadScreen;
import com.Utilities.UsefulMethods;

import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;

public class StepUpload extends BaseClass{
	
	@Given("^User is on Home page$")
	public void user_is_on_Home_page() throws Throwable {
		UploadScreen upload = new UploadScreen(driver);
		test = report.createTest("Login Functionality");
		Thread.sleep(2000);
		if(upload.readTitle(driver).equalsIgnoreCase("Register"))
		{
			eReport("Pass", "Homepage is loaded successfully");
		}
		else {
			eReport("Fail", "Homepage is not loaded successfully");
		}
	}

	@Given("^navigate to upload page$")
	public void navigate_to_upload_page() throws Throwable {
		eReport("Pass", "Navigated to Upload screen");
	}

	@Then("^upload the document in the webpage$")
	public void upload_the_document_in_the_webpage() throws Throwable {
		UploadScreen upload = new UploadScreen(driver);
		test = report.createTest("Upload Functionality");
		upload.clickUpload(driver);
		Thread.sleep(2000);
		Robot robot = new Robot();
		robot.setAutoDelay(1000);
		StringSelection selection = new StringSelection(UsefulMethods.getConfigdata("copyfile"));
		Toolkit.getDefaultToolkit().getSystemClipboard().getContents(selection);
		robot.setAutoDelay(2000);
		robot.keyPress(KeyEvent.VK_CONTROL);
		robot.keyPress(KeyEvent.VK_V);
		robot.setAutoDelay(2000);
		robot.keyRelease(KeyEvent.VK_CONTROL);
		robot.keyRelease(KeyEvent.VK_V);
		robot.keyPress(KeyEvent.VK_ENTER);
		robot.keyRelease(KeyEvent.VK_ENTER);
		eReport("Pass", "Uploaded the file using ROBOT.....");
		System.out.println("Robot uploaded the file");
		
	}

	@Then("^Verify the upload$")
	public void verify_the_upload() throws Throwable {
		System.out.println("Robot Execution completed");
		
	}


	

}
