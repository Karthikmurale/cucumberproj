package com.StepDefinitions;

import static org.junit.Assert.assertTrue;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.interactions.Actions;

import com.Utilities.UsefulMethods;
import com.sun.tools.javac.resources.compiler;

import ch.qos.logback.core.joran.action.Action;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;

public class Steps {

	public WebDriver driver;
	com.Utilities.UsefulMethods methods;

	@Given("^User in on Home page using url \"([^\"]*)\"$")
	public void user_in_on_Home_page_using_url(String url) throws Throwable {
		System.setProperty("webdriver.chrome.driver", "E:\\Selenium Workspace\\Driver\\chromedriver_win32\\chromedriver.exe");
		driver = new ChromeDriver();
		driver.get(url);
		driver.manage().window().maximize();
		driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);

	}

	@Given("^Enter valid \"([^\"]*)\" and \"([^\"]*)\"$")
	public void enter_valid_and(String arg1, String arg2) throws Throwable {
		System.out.println("Entered Valid username and password");
	}

	@Then("^verify the title as \"([^\"]*)\"$")
	public void verify_the_title_as(String etitle) throws Throwable {
		String atitle = driver.getTitle();
		if (atitle.contains(etitle)) {
			assertTrue(true);
			System.out.println("Testcase Passed");
		} else {
			System.out.println("Testcase Failed");
		}

	}
	
	
	//Validating the Textbox
	
	@Given("^User is login to the application$")
	public void user_is_login_to_the_application() throws Throwable {
		System.setProperty("webdriver.chrome.driver", "E:\\Selenium Workspace\\Driver\\chromedriver_win32\\chromedriver.exe");
		driver = new ChromeDriver();
		driver.get("https://demoqa.com/");
		driver.manage().window().maximize();
		driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
	}

	@Given("^navigate to \"([^\"]*)\" screen$")
	public void navigate_to_screen(String arg1) throws Throwable {
		Thread.sleep(2000);
		driver.findElement(By.xpath("//a[text()='Keyboard Events Sample Form']")).click();
		System.out.println("Screeen navigated to Keyboard Events Sample form screen");
	}

	@Given("^enter the name \"([^\"]*)\" and \"([^\"]*)\" and \"([^\"]*)\"$")
	public void enter_the_name_and_and(String arg1, String arg2, String arg3) throws Throwable {
		WebElement userElem = driver.findElement(By.xpath("//input[@id='userName']"));
		WebElement currAddress = driver.findElement(By.xpath("//textarea[@id='currentAddress']"));
		WebElement permAddress = driver.findElement(By.xpath("//textarea[@id='permanentAddress']"));
		methods.enterText(userElem, arg1);
		methods.enterText(currAddress, arg2);
		methods.enterText(permAddress, arg3);
	}

	@Given("^clcik on submit button$")
	public void clcik_on_submit_button() throws Throwable {
		WebElement btnElement = driver.findElement(By.xpath("//input[@id='submit']"));
		btnElement.click();
		driver.switchTo().alert().accept();
		System.out.println("Validation completed");
	}
	
	@Then("^quit the browser$")
	public void quit_the_browser() throws Throwable {
		
		driver.close();
		driver.quit();
	}
	
	@Given("^select the location as \"([^\"]*)\"$")
	public void select_the_location_as(String arg1) throws Throwable {
		methods.selectRadio(driver, arg1);
	}

	@Given("^select the hotel rating combined as \"([^\"]*)\"$")
	public void select_the_hotel_rating_combined_as(String arg1) throws Throwable {
		methods.selectMultipleCheck(driver, arg1);
	}

	@Given("^navigate to \"([^\"]*)\" page$")
	public void navigate_to_page(String arg1) throws Throwable {
		Thread.sleep(2000);
		driver.findElement(By.xpath("//a[text()='Checkboxradio']")).click();
		System.out.println("Screeen navigated to Checkboxradio screen");
	}
	
	//Validating the testdata from excel sheet
	@Given("^User is on homepage using \"([^\"]*)\"$")
	public void user_is_on_homepage_using(String arg1) throws Throwable {
		System.setProperty("webdriver.chrome.driver", "E:\\Selenium Workspace\\Driver\\chromedriver_win32\\chromedriver.exe");
		driver = new ChromeDriver();
		String url = UsefulMethods.getExceldata("Test", arg1);
		driver.get(url);
		driver.manage().window().maximize();
		driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
	}

	@Given("^navigated to \"([^\"]*)\"$")
	public void navigated_to(String arg1) throws Throwable {
		
		String[] menu = arg1.split(",");
		Actions actions = new Actions(driver);
		actions.clickAndHold(driver.findElement(By.xpath("//a[contains(text(),'About')]"))).build().perform();
		Thread.sleep(1000);
		actions.clickAndHold(driver.findElement(By.xpath("//a[contains(text(),'Glossary')]"))).build().perform();
		actions.click();
		Thread.sleep(1000);
		System.out.println("Sub-Menu selected succe3ssfully");
		UsefulMethods.writeExcelData("Test", "status","done");
	}
}
