package com.Utilities;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Properties;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.io.FileHandler;

import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

public class UsefulMethods {
	
	public static XSSFWorkbook wb;
	
	//Fetching the datas from properties file
	public static String getConfigdata(String key) throws IOException
	{
		String data = null;
		File file = new File(System.getProperty("user.dir")+File.separator+"config"+File.separator+"config.properties");
		FileInputStream fis = new FileInputStream(file);
		Properties prop = new Properties();
		prop.load(fis);
		if(!key.equalsIgnoreCase(""))
		{
			data =prop.getProperty(key).trim();
		}
		else {
			System.out.println("Incorrect Input to retrieve data from property file");
		}
		
		return data;
	}
	
	//Taking Screenshot
	public static String takeSnap(WebDriver driver) throws IOException
	{
		DateFormat dateFormat = new SimpleDateFormat("yyMMddHHmmss");
		Date date = new Date();
		String date1= dateFormat.format(date);
		File srcFile = ((TakesScreenshot)driver).getScreenshotAs(OutputType.FILE);
		String snappath= System.getProperty("user.dir")+File.separator+"screenshots"+File.separator+"Result"+date1+".png";
		File destFile= new File(snappath);
		FileHandler.copy(srcFile, destFile);
		System.out.println("Screenshot has been taken successfully");
		return snappath;
	}
	
	//ExtentReprot Declaration
	/*
	 * public static void ExtentReport(ExtentHtmlReporter reporter,ExtentReports
	 * report) {
	 * 
	 * }
	 */
	public static void enterText(WebElement element,String value)
	{
		if(element.isDisplayed())
		{
			element.clear();
			element.sendKeys(value.trim());
			element.sendKeys(Keys.TAB);
			String optValueString = element.getAttribute("id");
			System.out.println("The Value successfully registered in "+optValueString);
		}
		else {
			System.out.println("Required webelement is not displayed in application");
		}
	}
	
	
	public static void selectRadio(WebDriver driver, String value)
	{
		int i = driver.findElements(By.xpath("//label[text()='"+value+"']")).size();
		if(i>0)
		{
			int select  = driver.findElements(By.xpath("//label[@class='ui-checkboxradio-label ui-corner-all ui-button ui-widget ui-checkboxradio-checked ui-state-active' and text()='"+value+"']")).size();
			
			if(select>0) {
				System.out.println("The Required webelement is already selected");
			}
			else {
				System.out.println("The Required webelement is already not selected");
				driver.findElement(By.xpath("//label[text()='"+value+"']")).click();
				System.out.println("The Required webelement is selected Successfully");
			}
		}
	}
	
	
	public static void selectMultipleCheck(WebDriver driver, String value)
	{
		String[] chkMultString = value.trim().split(",");
		for(String option:chkMultString)
		{
			int opt = driver.findElements(By.xpath("//label[text()='"+option+"']")).size();
			if(opt>0)
			{
				int select  = driver.findElements(By.xpath("//label[@class='ui-checkboxradio-label ui-corner-all ui-button ui-widget ui-checkboxradio-checked ui-state-active' and text()='"+option+"']")).size();
				if(select>0) {
					System.out.println("The Required webelement is already selected");
				} else {
					System.out.println("The Required webelement is already not selected");
					driver.findElement(By.xpath("//label[text()='"+option+"']")).click();
					System.out.println("The Required webelement is selected Successfully");
				}
			}
			else {
				System.out.println("Required webelement is not displayed in the application");
			}
		}
	}
	
	public static String getExceldata(String Sheet,String key) throws IOException
	{
		String output = null;
		FileInputStream fis = null;
		File file = new File("E:\\Selenium Workspace\\cucumberproj\\TestData\\Sheets.xlsx");
		fis = new FileInputStream(file);
		{
			wb = new XSSFWorkbook(fis);
			XSSFSheet sht = wb.getSheet(Sheet);
			int size = sht.getLastRowNum() - sht.getFirstRowNum();
			for(int i=2;i<=size;i++)
			{
				if(sht.getRow(i).getCell(1).getStringCellValue().equalsIgnoreCase(key))
				{
					output = sht.getRow(i).getCell(2).getStringCellValue();
					break;
				}
			}
		}
		return output;
		
	}
	
	public static void writeExcelData(String Sheet,String key,String value) throws IOException
	{
		String output = null;
		File file = new File(System.getProperty("User.dir")+File.separator+"TestData"+File.separator+"TestData.xlsx");
		System.out.println(file);
		FileInputStream fis = new FileInputStream(file);
		if(file.exists())
		{
			FileOutputStream fos = new FileOutputStream(file);
			wb = new XSSFWorkbook(fis);
			XSSFSheet sht = wb.getSheet(Sheet);
			int size = sht.getLastRowNum() - sht.getFirstRowNum();
			for(int i=2;i<=size;i++)
			{
				if(sht.getRow(i).getCell(1).getStringCellValue().equalsIgnoreCase(key))
				{
					sht.createRow(i).createCell(2).setCellValue(value);
					wb.write(fos);
					System.out.println("The given value"+key+"is successfully written in TestData sheet");
					break;
				}
			}
			wb.close();
		}
	}
	
	
	
	
}
