package com.PageObjects;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.CacheLookup;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

public class HomePage {
	
	WebDriver lDriver;
	
	public HomePage(WebDriver rDriver)
	{
		lDriver = rDriver;
		PageFactory.initElements(lDriver, this);
	}

	 @FindBy(how = How.XPATH, using = "//a[text()='Home']")
	 @CacheLookup
	 WebElement btnHomElement;
	 

}
