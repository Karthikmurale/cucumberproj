package com.PageObjects;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.CacheLookup;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

public class UploadScreen {
	
WebDriver lDriver;
	
	public UploadScreen(WebDriver rDriver)
	{
		lDriver = rDriver;
		PageFactory.initElements(lDriver, this);
	}
	
	@FindBy(how = How.ID, using = "imagesrc")
	 @CacheLookup
	 private WebElement btnUpload;
	
	public void clickUpload(WebDriver driver)
	{
		btnUpload.click();
	}
	
	public  String readTitle(WebDriver driver)
	{
		String title = driver.getTitle().trim();
		return title;
	}

}
