package com.PageObjects;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.CacheLookup;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;



public class NOPLoginPage {
	
WebDriver lDriver;
	
	//Declaring WebElements
	 @CacheLookup
	 @FindBy(how=How.ID,using="Username")
	 private WebElement txtbx_nopUserName;
	 
	 @CacheLookup
	 @FindBy(how=How.ID,using="Password")
	 private WebElement txtbx_nopPassword;
	 
	 @CacheLookup
	 @FindBy(how=How.XPATH,using="//input[@value='Log in']")
	 private WebElement btn_Login;
	
	 
	 public NOPLoginPage(WebDriver rDriver)
		{
			lDriver = rDriver;
			PageFactory.initElements(lDriver, this);
		}
	 
	 
	 public void enterUsername(String value)
	 {
		 txtbx_nopUserName.sendKeys(value);
	 }
	 
	 public void enterPassword(String value)
	 {
		 txtbx_nopPassword.sendKeys(value);
	 }
	 
	 public void btnClick()
	 {
		 btn_Login.click();
	 }

}
