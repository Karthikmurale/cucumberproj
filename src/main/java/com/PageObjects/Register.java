package com.PageObjects;

import java.sql.Driver;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.CacheLookup;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;


public class Register {
	
	WebDriver lDriver;
	
	 public Register(WebDriver rDriver)
		{
			lDriver = rDriver;
			PageFactory.initElements(lDriver, this);
		}
	 
	//Declaring WebElements
		 @CacheLookup
		 @FindBy(how=How.ID,using="FirstName")
		 private WebElement txtbx_FirstName;
		 
		 @CacheLookup
		 @FindBy(how=How.ID,using="LastName")
		 private WebElement txtbx_LastName;
		 
		 @CacheLookup
		 @FindBy(how=How.ID,using="Email")
		 private WebElement txtbx_Email;
		 
		 @CacheLookup
		 @FindBy(how=How.ID,using="ConfirmEmail")
		 private WebElement txtbx_ConfirmEmail;
		 
		 @CacheLookup
		 @FindBy(how=How.ID,using="Username")
		 private WebElement txtbx_Username;
		 
		 @CacheLookup
		 @FindBy(how=How.ID,using="check-availability-button")
		 private WebElement btn_checkavailabilitybutton;
		 
		 @CacheLookup
		 @FindBy(how=How.XPATH,using="//span[@class='ico-user sprite-image']")
		 private WebElement btn_userdetails;
		 
		 @CacheLookup
		 @FindBy(how=How.XPATH,using="//a[text()='Register']")
		 private WebElement btn_Register;
		
		
		 
		 
		 
		 public String titleName()
		 {
			 String titleString = lDriver.getTitle();
			 return titleString;
		 }
		 
		 
		 public void clickRegister() throws InterruptedException
		 {
			 Actions action = new Actions(lDriver);
			 action.clickAndHold(btn_userdetails);
			 btn_userdetails.click();
			 Thread.sleep(2000);
			 btn_Register.click();
		 }
		 
		 public void enterFirstName(String value)
		 {
			 txtbx_FirstName.sendKeys(value);
			 System.out.println("The First Name Entered is "+value);
		 }
		 
		 public void enterLastName(String value)
		 {
			 txtbx_LastName.sendKeys(value);
			 System.out.println("The Last Name Entered is "+value);
		 }
		 
		 public void enterEmail(String value)
		 {
			 txtbx_Email.sendKeys(value);
			 System.out.println("The Email Entered is "+value);
		 }
		 
		 public void enterConfirmEmail(String value)
		 {
			txtbx_ConfirmEmail.sendKeys(value);
			 System.out.println("The Confirm Email Entered is "+value);
		 }
		 
		 public void enterUsername(String value)
		 {
			txtbx_Username.sendKeys(value);
			 System.out.println("The Username Entered is "+value);
		 }
		 
		 public void clickCheckAvailability()
		 {
			 btn_checkavailabilitybutton.click();
			 System.out.println("Check Availability Button is clicked");
		 }
}
