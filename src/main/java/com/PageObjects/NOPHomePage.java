package com.PageObjects;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

public class NOPHomePage {
WebDriver lDriver;
	
	public NOPHomePage(WebDriver rDriver)
	{
		lDriver = rDriver;
		PageFactory.initElements(lDriver, this);
	}
	
	//Declaring webelements
	
	@FindBy(how=How.XPATH,using="//span[@class='ico-caret sprite-image']")
	 private WebElement img_Logoptions;
	
	
	@FindBy(how=How.XPATH,using="//a[text()='Log out']")
	 private WebElement optn_Logout;
	
	
	public void optionsclick()
	{
		img_Logoptions.click();
	}
	
	public void logoutClick()
	{
		optn_Logout.click();
	}
	

}
